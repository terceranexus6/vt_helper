# vt_helper

A project to help gathering yara rules using pro VT API options

## Getting started

This is meant to be used on Linux.

The [VT API](https://developers.virustotal.com/reference/overview) is needed, in my case I donwloaded the binary and the repo tools are meant to be use in the same root folder where vt is. if you use vt in the commands path (so you don't use `./`) then change line 17 in the [checker](https://gitlab.com/terceranexus6/vt_helper/-/blob/main/check_yara.sh). 

Also in the [check script](https://gitlab.com/terceranexus6/vt_helper/-/blob/main/check_yara.sh) change <MYAPIKEY> with your pro vt API key. Once this is done, save your hashes in a file (one hash per line) and execute:

```
./check_rules.sh myfilewithhashes
```

Once executed it will save a results with the yara ids and the rawn yara rules. You can clean them like this:

```
./clean_yara.sh myyarafile nameofthenewfile
```


