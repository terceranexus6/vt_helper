#!/bin/bash

get_yara () {
echo -e "Downloading the yara ruleset of $1\n"
curl --request GET --url https://www.virustotal.com/api/v3/yara_rulesets/{$1} --header 'x-apikey: <MYAPIKEY>'
echo -e "Done\n"
}

#creating an array for the hashes
declare -a ides
while IFS= read -r line; do ides[${#ides[@]}]="$line"; done < $1

#iterating the array of hashes
for i in "${ides[@]}"
do
	#saving the ruleset ids in a file
	./vt file $i | grep ruleset_id | cut -d'"' -f 2 >> lista
done

#cleaning the repeated ids
perl -ne 'print unless $seen{$_}++' lista > listofrulesets 
rm lista
echo -e "Donwloaded the yara ruleset IDs to listofrulesets, now lets get to the rules\n"

#iterate the list of ids to get the yaras
while IFS= read -r line2; do
	get_yara "$line2" >> yaras
	echo -e "\n" >> yaras
done < listofrulesets

mkdir results
mv listofrulesets results/listofrulesets
mv yaras results/yaras

echo -e "Donwloaded yara rules.\nBye"
